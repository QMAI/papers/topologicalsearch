import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as sparse
import scipy.sparse.linalg as sparse_linalg
import scipy.signal
import scipy
from scipy.fft import fft, fftfreq

def QRW_topo_2D_with_defect_fixangle(size_x, size_y, topo_defect_coord, thetaParams, thetaParamsDefect):
    '''thetaParams - parameters for topo phase 1,  
       thetaParamsDefect - parameters for topo phase 2 - on one defect site,
       topo_defect_coord - tuple of (x_array, y_array) defect coordinates
       
       coordinate agreement - 2*(size_y*x+y) - coordinate of x, y up spin
    '''
    lattice_unitary_op = sparse.lil_matrix((2*size_x*size_y, 2*size_x*size_y), dtype = complex)
    for i in range(2*size_x*size_y):
        lattice_unitary_op[i, i] = 1.0
    
    lattice_unitary_op.tocsc()
    
    #print(lattice_unitary_op.todense())
    
    rotation1 = sparse.lil_matrix((2*size_x*size_y,2*size_x*size_y), dtype=complex)
    rotation2 = sparse.lil_matrix((2*size_x*size_y,2*size_x*size_y), dtype=complex)
    
    #step 1 - rotation 
    for i in range(size_x):
        for j in range(size_y):
            if (i in topo_defect_coord[0]) and (j in topo_defect_coord[1]):
                T1, T2 = thetaParamsDefect #defect
            else:
                T1, T2 = thetaParams
                
            cos1 = np.cos(T1/2)
            cos2 = np.cos(T2/2)
            sin1 = np.sin(T1/2)
            sin2 = np.sin(T2/2)

        
            rotation1[2*(size_y*i+j), 2*(size_y*i+j)] = cos1
            rotation1[2*(size_y*i+j), 2*(size_y*i+j)+1] = -sin1
            rotation1[2*(size_y*i+j) + 1, 2*(size_y*i+j)] = sin1
            rotation1[2*(size_y*i+j)+1, 2*(size_y*i+j)+1] = cos1
        
            rotation2[2*(size_y*i+j), 2*(size_y*i+j)] = cos2
            rotation2[2*(size_y*i+j), 2*(size_y*i+j)+1] = -sin2
            rotation2[2*(size_y*i+j) + 1, 2*(size_y*i+j)] = sin2
            rotation2[2*(size_y*i+j) + 1, 2*(size_y*i+j)+1] = cos2
        
    rotation1.tocsc()
    rotation2.tocsc()
    lattice_unitary_op = rotation1.dot(lattice_unitary_op)
        
    #shift 1 - along x (we impose PBC)
    shift_matrix_1 = sparse.lil_matrix((2*size_x*size_y,2*size_x*size_y), dtype=complex)
    for i in range(size_x):
        for j in range(size_y):
            if i == 0:
                shift_matrix_1[2*(size_y*i+j), 2*(size_y*(size_x-1)+j)] = 1
                shift_matrix_1[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j) + 1] = 1
            elif i == size_x - 1:
                shift_matrix_1[2*(size_y*i+j), 2*(size_y*(i-1)+j)] = 1
                shift_matrix_1[2*(size_y*i+j) + 1, 2*(size_y*(0)+j) + 1] = 1
            else:
                shift_matrix_1[2*(size_y*i+j), 2*(size_y*(i-1)+j)] = 1
                shift_matrix_1[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j) + 1] = 1
    
    shift_matrix_1.tocsc()
    lattice_unitary_op = shift_matrix_1.dot(lattice_unitary_op)
    
    #print(shift_matrix_1.dot(shift_matrix_1.T.conj()).todense())
    
    #rotation 2
    lattice_unitary_op = rotation2.dot(lattice_unitary_op)
    
    #shift 2 - along y (we impose PBC):
    shift_matrix_2 = sparse.lil_matrix((2*size_x*size_y,2*size_x*size_y), dtype=complex)
    
    for i in range(size_x):
        for j in range(size_y):
            if j == 0:
                #print(i, j, 2*(size_y*i+j), 'j=0')
                shift_matrix_2[2*(size_y*i+j), 2*(size_y*i+size_y - 1)] = 1
                shift_matrix_2[2*(size_y*i+j) + 1, 2*(size_y*i+j + 1) + 1] = 1
            elif j == size_y - 1:
                #print(i, j, 2*(size_y*i+j),'j=sizey - 1')
                shift_matrix_2[2*(size_y*i+j), 2*(size_y*i+j - 1)] = 1
                shift_matrix_2[2*(size_y*i+j) + 1, 2*(size_y*i+0) + 1] = 1
            else:
                #print(i, j, 2*(size_y*i+j),'other')
                shift_matrix_2[2*(size_y*i+j), 2*(size_y*i+j-1)] = 1
                shift_matrix_2[2*(size_y*i+j) + 1, 2*(size_y*i+j+1) + 1] = 1
    
    shift_matrix_2.tocsc()
    lattice_unitary_op = shift_matrix_2.dot(lattice_unitary_op)
    
    #print(shift_matrix_2.todense())
    #print(shift_matrix_2.dot(shift_matrix_2.T.conj()).todense())
    
    #rotation 3 - analogue of rotation 1
    lattice_unitary_op = rotation1.dot(lattice_unitary_op)
    
    #shift 3 - along y with pbc, and additional along x with pbc:
    shift_matrix_3 = sparse.lil_matrix((2*size_x*size_y,2*size_x*size_y), dtype=complex)
    
    for i in range(size_x):
        for j in range(size_y):
            if (i == 0) and (j == 0):
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(size_x-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+size_y-1) + 1] = 1
            elif (i == size_x - 1) and (j == size_y - 1):
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+0)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(0)+j-1) + 1] = 1
            elif (i == 0) and (j == size_y - 1):   
                ##
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(size_x-1)+0)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j-1) + 1] = 1
            elif (i == size_x - 1) and (j == 0):   
                ##
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(0)+size_y-1) + 1] = 1    
            elif j == 0:
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+size_y-1) + 1] = 1
            elif j == size_y - 1:
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+0)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j-1) + 1] = 1
            elif i == 0:
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(size_x-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j-1) + 1] = 1
            elif i == size_x - 1:
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(0)+j-1) + 1] = 1
            else:
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j-1) + 1] = 1
    
    shift_matrix_3.tocsc()
    lattice_unitary_op = shift_matrix_3.dot(lattice_unitary_op)
    
    #print(shift_matrix_3.dot(shift_matrix_3.T.conj()).todense())
    
    return lattice_unitary_op


def spectrum_plus(kx,ky,Theta1,Theta2):
    b = 2*np.cos(Theta2)*(np.sin(Theta1)**2*np.cos(2*ky)-np.cos(Theta1)**2*np.cos(kx))+np.sin(2*Theta1)*np.sin(Theta2)*\
    (np.cos(kx-2*ky)+1)
    eigenenergy = np.angle(-b+np.sqrt(b**2-(4+0.00000001j)*np.ones(len(b))))
    return eigenenergy


def max_probability_10_1000(size, thetaParams, thetaParamsDefect, single_defect_pos):
    '''returns max probabiltiy on defect and on 0 0, 0 1 points'''
    sizex = size
    sizey = size

   
    psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
    #print(abs(psi[8])**2+abs(psi[9])**2)
      
    time_set = range(1000)
    probabilities = np.zeros(len(time_set))
    probabilities_no_defect1 = np.zeros(len(time_set))
    probabilities_no_defect2 = np.zeros(len(time_set))

    unitary_operator = QRW_topo_2D_with_defect_fixangle(size_x=sizex, size_y=sizey, topo_defect_coord = single_defect_pos, 
                                           thetaParams = thetaParams, thetaParamsDefect = thetaParamsDefect)

    for i in time_set:
        psi = unitary_operator.dot(psi)
        probabilities[i] = np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])])**2+\
                           np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])+1])**2

        probabilities_no_defect1[i] = np.abs(psi[0])**2+np.abs(psi[1])**2
        probabilities_no_defect2[i] = np.abs(psi[2])**2+np.abs(psi[3])**2
        
    return np.max(probabilities[10:1000-1]), np.max(probabilities_no_defect1[10:1000-1]), np.max(probabilities_no_defect2[10:1000-1])


def dens_radius(matrix, r):
    '''
    r: radius
    matrix: density matrix of the state
    
    '''
    x = np.arange(0, matrix.shape[0])  # dimension of the mask
    y = np.arange(0, matrix.shape[1])

    center = np.unravel_index(np.argmax(matrix), matrix.shape)
    center = np.array(center)
    cx = center[0] # x-asix of mask center
    cy = center[1] # y-asix of mask center

    # Create a mask that covers a circle of area in the density matrix.
    # The field mask has the dimensions of matrix and has the value True 
    # if the condition on the righthand side is satisfied, and False otherwise. 
    mask = (x[np.newaxis,:]-cx)**2 + (y[:,np.newaxis]-cy)**2 < r**2
    surface = sum(matrix[mask])
    return surface

def get_radius(state_index, size, condition):
    sizex = sizey = size
    defect_loc = [26, 25] #change defect position to 16, 15 for size below 26
    single_defect_pos = ([26], [25])
    dofect_correction = 0
    
    unitary_operator = QRW_topo_2D_with_defect_fixangle(size_x=sizex, size_y=sizey, topo_defect_coord = single_defect_pos, 
                                               thetaParams = [0.8377580409572776, 3.853686988403478], 
                                               thetaParamsDefect = [1.9891491173483118+0.3*dofect_correction, np.pi/2])
    
    eigvals, eigvecs = np.linalg.eig(unitary_operator.todense())
    quasi_energies1 = np.angle(eigvals)
    sort_indices_quasi_energies = np.argsort(quasi_energies1)
    eigvecs_sorted = np.array(eigvecs)[:,sort_indices_quasi_energies]
    # quasi_energies1_sorted = quasi_energies1[sort_indices_quasi_energies]
    # eigvals_reordered = eigvals[sort_indices_quasi_energies]
    
    state = eigvecs_sorted[:,state_index]
    density_matrix = np.zeros((sizex, sizey))
    
    for i in range(sizex):
        for j in range(sizey):
            density_matrix[i,j]=np.abs(state[2*(sizex*i+j)])**2+np.abs(state[2*(sizex*i+j)+1])**2
                    
    cent = np.unravel_index(np.argmax(density_matrix), density_matrix.shape)
    cent = np.array(cent)
    
    ## conditionals that satisfied the density condition
    
    radiuses = np.arange(0,30)
    for radius in radiuses:
        if dens_radius(density_matrix, r = radius) >= condition:
            print("radius is {} given condition {}".format(radius,condition))
            
            break

    return radius