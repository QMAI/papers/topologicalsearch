import numpy as np
import scipy.sparse as sparse
import scipy.sparse.linalg as sparse_linalg
import scipy.signal
import scipy
from scipy.fft import fft, fftfreq
from numpy import random
import time

#supplementary functions
def kron_power(U,n):
    """
    Entries :
        U -> A numpy matrix.
        n -> An integer.
    Output :
        U to the kronecker power n.
    """
    tmp = np.array([[1.]])
    for i in range(n):
        tmp = np.kron(tmp,U)
    return tmp

def dot_multiple(l):
    """
    Entry :
        l -> A list of numpy matricies.
    Output :
        The dot product of all the matricies in l.
    """
    assert(len(l)!=0)
    tmp = np.eye(len(l[0]))
    for i in range(len(l)):
        tmp = np.dot(tmp,l[i])
    return tmp

def get_proba(psi):
    """
    Entry :
        psi -> The state of the quantum walk.
    Output :
        The probability to be in each position of the grid in the form of a matrix of size NxN$
    """
    N = int(np.sqrt(len(psi)/2))
    l = np.array([np.abs(psi[i])**2+np.abs(psi[N*N+i])**2 for i in range(N*N)])
    return np.reshape(l,(N,N))


#main functions for Grover
def get_matrix_qw_Grover_disordered(nb,search=[], disorder_strength = 0.01, seed_value=0):
    """
    Entries :
        nb -> The size parameter of the walk.
        search -> The list of searched elements. Range from 0 to N^2-1.
        disorder_strength -> in Cx phase and Cy phase
    Outputs : {1},{2}
        {1} -> The operator U of the walk.
        {2} -> The operator U' of the search (walk and oracle).
        
    Randomization is introduced into Cx operator as phase
    """
    
    def mod_number(x,N):
        l = list(map(int,list(bin(x)[2:])))
        l = [0]*(N-len(l))+l
        l = [l[len(l)-i-1] for i in range(len(l))]
        y = 0
        for i in range(N):
            y += l[(i+N//2)%N]*2**i
        return y
    

    N = 2**nb
    #N = nb

    X = np.array([[1 if (i-j+N)%N == 1 else 0 for j in range(N)] for i in range(N)],dtype=float)
    sig = np.array([[X[i,j] if i<N and j<N else (X[j-N][i-N]if i>=N and j>=N else 0) for j in range(2*N)] for i in range(2*N)],dtype=float)
    sig_sparse = sparse.csc_matrix(sig)
    ones_sparse = sparse.csc_matrix(sparse.eye(N))
    U_trans_sparse = sparse.kron(sig_sparse, ones_sparse, format='csc')
    #U_trans = np.kron(sig,np.eye(N)) # sig \otimes I_N
    #U_trans_sparse = sparse.csc_matrix(U_trans)
    del sig, X

    
    #SW = np.array([[1 if mod_number(i,2*nb)==j else 0 for j in range(N*N)] for i in range(N*N)],dtype=float)
    #it is possible to replace with faster version because mod_number(i,2*nb) = i//2**nb + (i % 2**nb)*2**nb
    U_swap_sparse = sparse.lil_matrix((2*N*N,2*N*N), dtype=float)
    for i in range(N*N):
        U_swap_sparse[i, i//N + (i % N) * N] = 1
        U_swap_sparse[N*N+i, N*N + i//N + (i % N) * N] = 1
        
    #U_swap = np.kron(np.eye(2),SW) # I_2 \otimes SW
    U_swap_sparse.tocsc()
    #del U_swap, SW
    
    #here old version replaced by randomized
    random.seed(seed_value)
    random_theta_disorder = disorder_strength * (random.rand(N*N)-0.5*np.ones(N*N))
    random_theta_disorder2 = disorder_strength * (random.rand(N*N)-0.5*np.ones(N*N))
    
    U_Xcoin_sparse = sparse.kron(np.array([[1,0],[0,1]])/np.sqrt(2), sparse.eye(N*N))
    U_Xcoin_sparse += sparse.kron(np.array([[0,1j],[0,0]])/np.sqrt(2), sparse.diags(np.exp(1j*random_theta_disorder)))
    U_Xcoin_sparse += sparse.kron(np.array([[0,0],[1j,0]])/np.sqrt(2), sparse.diags(np.exp(-1j*random_theta_disorder)))
    #Cx = np.array([[1,1j],[1j,1]])/np.sqrt(2) # Coin X
    #U_Xcoin_sparse = sparse.kron(Cx,sparse.eye(N*N))
    U_Xcoin_sparse.tocsc()
    
    U_Ycoin_sparse = sparse.kron(np.array([[1,0],[0,1]])/np.sqrt(2), sparse.eye(N*N))
    U_Ycoin_sparse += sparse.kron(np.array([[0,-1j],[0,0]])/np.sqrt(2), sparse.diags(np.exp(1j*random_theta_disorder2)))
    U_Ycoin_sparse += sparse.kron(np.array([[0,0],[-1j,0]])/np.sqrt(2), sparse.diags(np.exp(-1j*random_theta_disorder2)))
    #Cy = np.array([[1,-1j],[-1j,1]])/np.sqrt(2) # Coin Y
    #U_Ycoin_sparse = sparse.kron(Cy,sparse.eye(N*N))
    U_Ycoin_sparse.tocsc() # = sparse.csc_matrix(U_Ycoin)
    #del Cy, U_Ycoin
    
    #U = dot_multiple([U_swap,U_trans,U_swap,U_Ycoin,U_trans,U_Xcoin])
    U = U_swap_sparse.dot(U_trans_sparse).dot(U_swap_sparse).dot(U_Ycoin_sparse).dot(U_trans_sparse).dot(U_Xcoin_sparse)
    
    R = sparse.eye(N*N,dtype=complex)
    for i in search:
        R[i,i] = -1.
    U_R = sparse.kron(sparse.eye(2),R) # Oracle
    
    return U


def simul_Grover_disordered(nb,search,m=-1, disorder_strength = 0.01, seed_value=0):
    """
    Entries :
        nb -> The size parameter of the walk.
        search -> The list of searched elements. Range from 0 to N^2-1.
        m -> The number of steps before measuring. If -1 or nothing is given, the theoretical hitting time is used.
    Output : {1},{2}
        [1} -> The list of the steps for which we make a measurement.
        {2} -> The probability of success for each step between 0 and m included.
    """
    #global U_save
    N = 2**nb
    #N = nb
    
    if m==-1:
        m = int(np.round((pi/4.)*N*np.sqrt(2*0.33*np.log(N))-0.33*np.log(N**2),0)) # Hitting time
    
    #while nb > len(U_save):
    #tmp,_,_,_,_,_,_,_,_,_,_,_ = get_matrix_qw(nb,[])
    #U_save.append(tmp)
    #U_save,_,_,_,_,_,_,_,_,_,_,_ = get_matrix_qw(nb,[])
    U_save =  get_matrix_qw_Grover_disordered(nb,search=[], disorder_strength = disorder_strength, seed_value=seed_value)
    
    #R = np.eye(N*N,dtype=complex)
    #for i in search:
    #    R[i,i] = -1.
    #U_R = np.kron(np.eye(2),R)
    #U = np.dot(U_save,U_R) # Search operator

    U_R = sparse.lil_matrix((2*N*N,2*N*N), dtype=complex)
    for i in range(N*N):
        if i in search:
            U_R[i,i] = -1
            U_R[N*N+i,N*N+i] = -1
        else:
            U_R[i,i] = 1
            U_R[N*N+i,N*N+i] = 1
    U = U_save.dot(U_R) # Search operator
    
    M=list(range(0,m+1))
    p=np.zeros(len(M))
    
    psi = np.array([1./np.sqrt(2*N*N)]*(2*N*N)) # Initial state
    res = get_proba(psi)
    p[0] = sum([res[i//N,i%N] for i in search])
    
    for i in range(1,m+1):
        #M.append(i)
        #psi = np.dot(U,psi)
        psi = U.dot(psi)
        res = get_proba(psi)
        p[i-1] = sum([res[i//N,i%N] for i in search])
    return M,p

start = time.time()

oracle = [2]
maxtime = 5000
m,p = simul_Grover_disordered(11,oracle,maxtime, disorder_strength = 0.0)

print('time_11 = ', list(m))
print('probabilities_11 = ', list(p))
print(time.time() - start)

oracle = [2]
maxtime = 3000
m,p = simul_Grover_disordered(10,oracle,maxtime, disorder_strength = 0.0)

print('time_10 = ', list(m))
print('probabilities_10 = ', list(p))
print(time.time() - start)

oracle = [2]
maxtime = 8000
m,p = simul_Grover_disordered(12,oracle,maxtime, disorder_strength = 0.0)

print('time_12 = ', list(m))
print('probabilities_12 = ', list(p))
print(time.time() - start)