import numpy as np
import scipy.sparse as sparse
import scipy.sparse.linalg as sparse_linalg
import scipy
from numpy import random
#additional
import time


def QRW_topo_2D_with_defect_fixangle_disorder(size_x, size_y, topo_defect_coord, thetaParams, thetaParamsDefect, 
                                              disorder_strength = 0.01, seed_value=0):
    '''thetaParams - parameters for topo phase 1,  
       thetaParamsDefect - parameters for topo phase 2 - on one defect site,
       topo_defect_coord - tuple of (x_array, y_array) defect coordinates
       
       coordinate agreement - 2*(size_y*x+y) - coordinate of x, y up spin
    '''
    lattice_unitary_op = sparse.lil_matrix((2*size_x*size_y, 2*size_x*size_y), dtype = complex)
    for i in range(2*size_x*size_y):
        lattice_unitary_op[i, i] = 1.0
    
    lattice_unitary_op.tocsc()
    
    #print(lattice_unitary_op.todense())
    
    rotation1 = sparse.lil_matrix((2*size_x*size_y,2*size_x*size_y), dtype=complex)
    rotation2 = sparse.lil_matrix((2*size_x*size_y,2*size_x*size_y), dtype=complex)
    
    #step 1 - rotation 
    random.seed(seed_value)
    random_theta_disorder = disorder_strength * (random.rand(2*size_x*size_y)-0.5*np.ones(2*size_x*size_y))
    for i in range(size_x):
        for j in range(size_y):
            if (i in topo_defect_coord[0]) and (j in topo_defect_coord[1]):
                T1, T2 = thetaParamsDefect #defect
            else:
                T1, T2 = thetaParams 
                T1 = T1 + random_theta_disorder[2*(i*size_y+j)]
                T2 = T2 + random_theta_disorder[2*(i*size_y+j)+1]
                
            cos1 = np.cos(T1/2)
            cos2 = np.cos(T2/2)
            sin1 = np.sin(T1/2)
            sin2 = np.sin(T2/2)

        
            rotation1[2*(size_y*i+j), 2*(size_y*i+j)] = cos1
            rotation1[2*(size_y*i+j), 2*(size_y*i+j)+1] = -sin1
            rotation1[2*(size_y*i+j) + 1, 2*(size_y*i+j)] = sin1
            rotation1[2*(size_y*i+j)+1, 2*(size_y*i+j)+1] = cos1
        
            rotation2[2*(size_y*i+j), 2*(size_y*i+j)] = cos2
            rotation2[2*(size_y*i+j), 2*(size_y*i+j)+1] = -sin2
            rotation2[2*(size_y*i+j) + 1, 2*(size_y*i+j)] = sin2
            rotation2[2*(size_y*i+j) + 1, 2*(size_y*i+j)+1] = cos2
        
    rotation1.tocsc()
    rotation2.tocsc()
    lattice_unitary_op = rotation1.dot(lattice_unitary_op)
        
    #shift 1 - along x (we impose PBC)
    shift_matrix_1 = sparse.lil_matrix((2*size_x*size_y,2*size_x*size_y), dtype=complex)
    for i in range(size_x):
        for j in range(size_y):
            if i == 0:
                shift_matrix_1[2*(size_y*i+j), 2*(size_y*(size_x-1)+j)] = 1
                shift_matrix_1[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j) + 1] = 1
            elif i == size_x - 1:
                shift_matrix_1[2*(size_y*i+j), 2*(size_y*(i-1)+j)] = 1
                shift_matrix_1[2*(size_y*i+j) + 1, 2*(size_y*(0)+j) + 1] = 1
            else:
                shift_matrix_1[2*(size_y*i+j), 2*(size_y*(i-1)+j)] = 1
                shift_matrix_1[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j) + 1] = 1
    
    shift_matrix_1.tocsc()
    lattice_unitary_op = shift_matrix_1.dot(lattice_unitary_op)
    
    #print(shift_matrix_1.dot(shift_matrix_1.T.conj()).todense())
    
    #rotation 2
    lattice_unitary_op = rotation2.dot(lattice_unitary_op)
    
    #shift 2 - along y (we impose PBC):
    shift_matrix_2 = sparse.lil_matrix((2*size_x*size_y,2*size_x*size_y), dtype=complex)
    
    for i in range(size_x):
        for j in range(size_y):
            if j == 0:
                #print(i, j, 2*(size_y*i+j), 'j=0')
                shift_matrix_2[2*(size_y*i+j), 2*(size_y*i+size_y - 1)] = 1
                shift_matrix_2[2*(size_y*i+j) + 1, 2*(size_y*i+j + 1) + 1] = 1
            elif j == size_y - 1:
                #print(i, j, 2*(size_y*i+j),'j=sizey - 1')
                shift_matrix_2[2*(size_y*i+j), 2*(size_y*i+j - 1)] = 1
                shift_matrix_2[2*(size_y*i+j) + 1, 2*(size_y*i+0) + 1] = 1
            else:
                #print(i, j, 2*(size_y*i+j),'other')
                shift_matrix_2[2*(size_y*i+j), 2*(size_y*i+j-1)] = 1
                shift_matrix_2[2*(size_y*i+j) + 1, 2*(size_y*i+j+1) + 1] = 1
    
    shift_matrix_2.tocsc()
    lattice_unitary_op = shift_matrix_2.dot(lattice_unitary_op)
    
    #print(shift_matrix_2.todense())
    #print(shift_matrix_2.dot(shift_matrix_2.T.conj()).todense())
    
    #rotation 3 - analogue of rotation 1
    lattice_unitary_op = rotation1.dot(lattice_unitary_op)
    
    #shift 3 - along y with pbc, and additional along x with pbc:
    shift_matrix_3 = sparse.lil_matrix((2*size_x*size_y,2*size_x*size_y), dtype=complex)
    
    for i in range(size_x):
        for j in range(size_y):
            if (i == 0) and (j == 0):
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(size_x-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+size_y-1) + 1] = 1
            elif (i == size_x - 1) and (j == size_y - 1):
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+0)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(0)+j-1) + 1] = 1
            elif (i == 0) and (j == size_y - 1):   
                ##
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(size_x-1)+0)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j-1) + 1] = 1
            elif (i == size_x - 1) and (j == 0):   
                ##
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(0)+size_y-1) + 1] = 1    
            elif j == 0:
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+size_y-1) + 1] = 1
            elif j == size_y - 1:
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+0)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j-1) + 1] = 1
            elif i == 0:
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(size_x-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j-1) + 1] = 1
            elif i == size_x - 1:
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(0)+j-1) + 1] = 1
            else:
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j-1) + 1] = 1
    
    shift_matrix_3.tocsc()
    lattice_unitary_op = shift_matrix_3.dot(lattice_unitary_op)
    
    #print(shift_matrix_3.dot(shift_matrix_3.T.conj()).todense())
    
    return lattice_unitary_op


#supplementary functions
def kron_power(U,n):
    """
    Entries :
        U -> A numpy matrix.
        n -> An integer.
    Output :
        U to the kronecker power n.
    """
    tmp = np.array([[1.]])
    for i in range(n):
        tmp = np.kron(tmp,U)
    return tmp

def dot_multiple(l):
    """
    Entry :
        l -> A list of numpy matricies.
    Output :
        The dot product of all the matricies in l.
    """
    assert(len(l)!=0)
    tmp = np.eye(len(l[0]))
    for i in range(len(l)):
        tmp = np.dot(tmp,l[i])
    return tmp

def get_proba(psi):
    """
    Entry :
        psi -> The state of the quantum walk.
    Output :
        The probability to be in each position of the grid in the form of a matrix of size NxN$
    """
    N = int(np.sqrt(len(psi)/2))
    l = np.array([np.abs(psi[i])**2+np.abs(psi[N*N+i])**2 for i in range(N*N)])
    return np.reshape(l,(N,N))


#main functions for Grover
def get_matrix_qw_Grover_disordered(nb,search=[], disorder_strength = 0.01, seed_value=0):
    """
    Entries :
        nb -> The size parameter of the walk.
        search -> The list of searched elements. Range from 0 to N^2-1.
        disorder_strength -> in Cx phase and Cy phase
    Outputs : {1},{2}
        {1} -> The operator U of the walk.
        {2} -> The operator U' of the search (walk and oracle).
        
    Randomization is introduced into Cx operator as phase
    """
    
    def mod_number(x,N):
        l = list(map(int,list(bin(x)[2:])))
        l = [0]*(N-len(l))+l
        l = [l[len(l)-i-1] for i in range(len(l))]
        y = 0
        for i in range(N):
            y += l[(i+N//2)%N]*2**i
        return y
    

    N = 2**nb
    #N = nb

    X = np.array([[1 if (i-j+N)%N == 1 else 0 for j in range(N)] for i in range(N)],dtype=float)
    sig = np.array([[X[i,j] if i<N and j<N else (X[j-N][i-N]if i>=N and j>=N else 0) for j in range(2*N)] for i in range(2*N)],dtype=float)
    sig_sparse = sparse.csc_matrix(sig)
    ones_sparse = sparse.csc_matrix(sparse.eye(N))
    U_trans_sparse = sparse.kron(sig_sparse, ones_sparse, format='csc')
    #U_trans = np.kron(sig,np.eye(N)) # sig \otimes I_N
    #U_trans_sparse = sparse.csc_matrix(U_trans)
    del sig, X

    
    #SW = np.array([[1 if mod_number(i,2*nb)==j else 0 for j in range(N*N)] for i in range(N*N)],dtype=float)
    #it is possible to replace with faster version because mod_number(i,2*nb) = i//2**nb + (i % 2**nb)*2**nb
    U_swap_sparse = sparse.lil_matrix((2*N*N,2*N*N), dtype=float)
    for i in range(N*N):
        U_swap_sparse[i, i//N + (i % N) * N] = 1
        U_swap_sparse[N*N+i, N*N + i//N + (i % N) * N] = 1
        
    #U_swap = np.kron(np.eye(2),SW) # I_2 \otimes SW
    U_swap_sparse.tocsc()
    #del U_swap, SW
    
    #here old version replaced by randomized
    random.seed(seed_value)
    random_theta_disorder = disorder_strength * (random.rand(N*N)-0.5*np.ones(N*N))
    random_theta_disorder2 = disorder_strength * (random.rand(N*N)-0.5*np.ones(N*N))
    
    U_Xcoin_sparse = sparse.kron(np.array([[1,0],[0,1]])/np.sqrt(2), sparse.eye(N*N))
    U_Xcoin_sparse += sparse.kron(np.array([[0,1j],[0,0]])/np.sqrt(2), sparse.diags(np.exp(1j*random_theta_disorder)))
    U_Xcoin_sparse += sparse.kron(np.array([[0,0],[1j,0]])/np.sqrt(2), sparse.diags(np.exp(-1j*random_theta_disorder)))
    #Cx = np.array([[1,1j],[1j,1]])/np.sqrt(2) # Coin X
    #U_Xcoin_sparse = sparse.kron(Cx,sparse.eye(N*N))
    U_Xcoin_sparse.tocsc()
    
    U_Ycoin_sparse = sparse.kron(np.array([[1,0],[0,1]])/np.sqrt(2), sparse.eye(N*N))
    U_Ycoin_sparse += sparse.kron(np.array([[0,-1j],[0,0]])/np.sqrt(2), sparse.diags(np.exp(1j*random_theta_disorder2)))
    U_Ycoin_sparse += sparse.kron(np.array([[0,0],[-1j,0]])/np.sqrt(2), sparse.diags(np.exp(-1j*random_theta_disorder2)))
    #Cy = np.array([[1,-1j],[-1j,1]])/np.sqrt(2) # Coin Y
    #U_Ycoin_sparse = sparse.kron(Cy,sparse.eye(N*N))
    U_Ycoin_sparse.tocsc() # = sparse.csc_matrix(U_Ycoin)
    #del Cy, U_Ycoin
    
    #U = dot_multiple([U_swap,U_trans,U_swap,U_Ycoin,U_trans,U_Xcoin])
    U = U_swap_sparse.dot(U_trans_sparse).dot(U_swap_sparse).dot(U_Ycoin_sparse).dot(U_trans_sparse).dot(U_Xcoin_sparse)
    
    R = sparse.eye(N*N,dtype=complex)
    for i in search:
        R[i,i] = -1.
    U_R = sparse.kron(sparse.eye(2),R) # Oracle
    
    return U


def simul_Grover_disordered(nb,search,m=-1, disorder_strength = 0.01, seed_value=0):
    """
    Entries :
        nb -> The size parameter of the walk.
        search -> The list of searched elements. Range from 0 to N^2-1.
        m -> The number of steps before measuring. If -1 or nothing is given, the theoretical hitting time is used.
    Output : {1},{2}
        [1} -> The list of the steps for which we make a measurement.
        {2} -> The probability of success for each step between 0 and m included.
    """
    #global U_save
    N = 2**nb
    #N = nb
    
    if m==-1:
        m = int(np.round((pi/4.)*N*np.sqrt(2*0.33*np.log(N))-0.33*np.log(N**2),0)) # Hitting time
    
    #while nb > len(U_save):
    #tmp,_,_,_,_,_,_,_,_,_,_,_ = get_matrix_qw(nb,[])
    #U_save.append(tmp)
    #U_save,_,_,_,_,_,_,_,_,_,_,_ = get_matrix_qw(nb,[])
    U_save =  get_matrix_qw_Grover_disordered(nb,search=[], disorder_strength = disorder_strength, seed_value=seed_value)
    
    #R = np.eye(N*N,dtype=complex)
    #for i in search:
    #    R[i,i] = -1.
    #U_R = np.kron(np.eye(2),R)
    #U = np.dot(U_save,U_R) # Search operator

    U_R = sparse.lil_matrix((2*N*N,2*N*N), dtype=complex)
    for i in range(N*N):
        if i in search:
            U_R[i,i] = -1
            U_R[N*N+i,N*N+i] = -1
        else:
            U_R[i,i] = 1
            U_R[N*N+i,N*N+i] = 1
    U = U_save.dot(U_R) # Search operator
    
    p=[]
    M=[]
    
    psi = np.array([1./np.sqrt(2*N*N)]*(2*N*N)) # Initial state
    M.append(0)
    res = get_proba(psi)
    p.append(sum([res[i//N,i%N] for i in search]))
    
    for i in range(1,m+1):
        M.append(i)
        #psi = np.dot(U,psi)
        psi = U.dot(psi)
        res = get_proba(psi)
        p.append(sum([res[i//N,i%N] for i in search]))
    return M,p



#main code parts
sizex = 256
sizey = 256
disorder_str = 0.3

seeds_total_count = 20
single_defect_pos = ([26], [25]) #change defect position to 16, 15 for size below 26


#psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
#print(abs(psi[8])**2+abs(psi[9])**2)

calc_time_start = time.time()

time_set = range(500)
probabilities = np.zeros(len(time_set))


for sv in range(seeds_total_count):
    psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
    unitary_operator = QRW_topo_2D_with_defect_fixangle_disorder(size_x=sizex, size_y=sizey, topo_defect_coord = single_defect_pos, 
                                           thetaParams = [0.8377580409572776, 3.853686988403478], 
                                           thetaParamsDefect = [5*np.pi/8, np.pi/2], disorder_strength=disorder_str, seed_value=sv)

    for i in time_set:
        psi = unitary_operator.dot(psi)
        probabilities[i] += np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])])**2+\
                       np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])+1])**2

print('proba_walker1_size256_disorder03=', list(probabilities/seeds_total_count))
      
time_set = range(500)
probabilities = np.zeros(len(time_set))

for sv in range(seeds_total_count):
    psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
    unitary_operator = QRW_topo_2D_with_defect_fixangle_disorder(size_x=sizex, size_y=sizey, topo_defect_coord = single_defect_pos, 
                                           thetaParams = [5.445427266222307, 1.591740277818828], 
                                           thetaParamsDefect = [5*np.pi/8, np.pi/2], disorder_strength=disorder_str, seed_value=sv)

    for i in time_set:
        psi = unitary_operator.dot(psi)
        probabilities[i] += np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])])**2+\
                       np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])+1])**2

print('proba_walker2_size256_disorder03=', list(probabilities/seeds_total_count))

finish_1_time = time.time()
print('done topo part size 256 in time =', finish_1_time -calc_time_start)

oracle = [2]
#maveraged = np.zeros(len(time_set))
paveraged = np.zeros(len(time_set)+1)
for sv in range(seeds_total_count):
    m,p = simul_Grover_disordered(8,oracle,len(time_set), disorder_strength = disorder_str, seed_value=sv)
    paveraged += np.array(p)
    #print('done grover seed=', sv)

print('m_grover=', list(m))
print('p_grover_size256_disorder03 =', list(paveraged/seeds_total_count))
finish_2_time = time.time()
print('done grover part size 256 in time =', finish_2_time -calc_time_start)


sizex = 256
sizey = 256
disorder_str = 0.5

seeds_total_count = 20
single_defect_pos = ([26], [25]) #change defect position to 16, 15 for size below 26


#psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
#print(abs(psi[8])**2+abs(psi[9])**2)

calc_time_start = time.time()

time_set = range(500)
probabilities = np.zeros(len(time_set))


for sv in range(seeds_total_count):
    psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
    unitary_operator = QRW_topo_2D_with_defect_fixangle_disorder(size_x=sizex, size_y=sizey, topo_defect_coord = single_defect_pos, 
                                           thetaParams = [0.8377580409572776, 3.853686988403478], 
                                           thetaParamsDefect = [5*np.pi/8, np.pi/2], disorder_strength=disorder_str, seed_value=sv)

    for i in time_set:
        psi = unitary_operator.dot(psi)
        probabilities[i] += np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])])**2+\
                       np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])+1])**2

print('proba_walker1_size256_disorder05=', list(probabilities/seeds_total_count))
      
time_set = range(500)
probabilities = np.zeros(len(time_set))

for sv in range(seeds_total_count):
    psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
    unitary_operator = QRW_topo_2D_with_defect_fixangle_disorder(size_x=sizex, size_y=sizey, topo_defect_coord = single_defect_pos, 
                                           thetaParams = [5.445427266222307, 1.591740277818828], 
                                           thetaParamsDefect = [5*np.pi/8, np.pi/2], disorder_strength=disorder_str, seed_value=sv)

    for i in time_set:
        psi = unitary_operator.dot(psi)
        probabilities[i] += np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])])**2+\
                       np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])+1])**2

print('proba_walker2_size256_disorder05=', list(probabilities/seeds_total_count))

finish_1_time = time.time()
print('done topo part size 256 dis 0.5 in time =', finish_1_time -calc_time_start)

oracle = [2]
#maveraged = np.zeros(len(time_set))
paveraged = np.zeros(len(time_set)+1)
for sv in range(seeds_total_count):
    m,p = simul_Grover_disordered(8,oracle,len(time_set), disorder_strength = disorder_str, seed_value=sv)
    paveraged += np.array(p)
    #print('done grover seed=', sv)

print('m_grover=', list(m))
print('p_grover_size256_disorder05 =', list(paveraged/seeds_total_count))
finish_2_time = time.time()
print('done grover part size 256 dis 0.5 in time =', finish_2_time -calc_time_start)



sizex = 256
sizey = 256
disorder_str = 0.75

seeds_total_count = 20
single_defect_pos = ([26], [25]) #change defect position to 16, 15 for size below 26


#psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
#print(abs(psi[8])**2+abs(psi[9])**2)

calc_time_start = time.time()

time_set = range(500)
probabilities = np.zeros(len(time_set))


for sv in range(seeds_total_count):
    psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
    unitary_operator = QRW_topo_2D_with_defect_fixangle_disorder(size_x=sizex, size_y=sizey, topo_defect_coord = single_defect_pos, 
                                           thetaParams = [0.8377580409572776, 3.853686988403478], 
                                           thetaParamsDefect = [5*np.pi/8, np.pi/2], disorder_strength=disorder_str, seed_value=sv)

    for i in time_set:
        psi = unitary_operator.dot(psi)
        probabilities[i] += np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])])**2+\
                       np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])+1])**2

print('proba_walker1_size256_disorder075=', list(probabilities/seeds_total_count))
      
time_set = range(500)
probabilities = np.zeros(len(time_set))

for sv in range(seeds_total_count):
    psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
    unitary_operator = QRW_topo_2D_with_defect_fixangle_disorder(size_x=sizex, size_y=sizey, topo_defect_coord = single_defect_pos, 
                                           thetaParams = [5.445427266222307, 1.591740277818828], 
                                           thetaParamsDefect = [5*np.pi/8, np.pi/2], disorder_strength=disorder_str, seed_value=sv)

    for i in time_set:
        psi = unitary_operator.dot(psi)
        probabilities[i] += np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])])**2+\
                       np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])+1])**2

print('proba_walker2_size256_disorder075=', list(probabilities/seeds_total_count))

finish_1_time = time.time()
print('done topo part size 256 dis 0.5 in time =', finish_1_time -calc_time_start)

oracle = [2]
#maveraged = np.zeros(len(time_set))
paveraged = np.zeros(len(time_set)+1)
for sv in range(seeds_total_count):
    m,p = simul_Grover_disordered(8,oracle,len(time_set), disorder_strength = disorder_str, seed_value=sv)
    paveraged += np.array(p)
    #print('done grover seed=', sv)

print('m_grover=', list(m))
print('p_grover_size256_disorder075 =', list(paveraged/seeds_total_count))
finish_2_time = time.time()
print('done grover part size 256 dis 0.5 in time =', finish_2_time -calc_time_start)



sizex = 256
sizey = 256
disorder_str = 1.0

seeds_total_count = 20
single_defect_pos = ([26], [25]) #change defect position to 16, 15 for size below 26


#psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
#print(abs(psi[8])**2+abs(psi[9])**2)

calc_time_start = time.time()

time_set = range(500)
probabilities = np.zeros(len(time_set))


for sv in range(seeds_total_count):
    psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
    unitary_operator = QRW_topo_2D_with_defect_fixangle_disorder(size_x=sizex, size_y=sizey, topo_defect_coord = single_defect_pos, 
                                           thetaParams = [0.8377580409572776, 3.853686988403478], 
                                           thetaParamsDefect = [5*np.pi/8, np.pi/2], disorder_strength=disorder_str, seed_value=sv)

    for i in time_set:
        psi = unitary_operator.dot(psi)
        probabilities[i] += np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])])**2+\
                       np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])+1])**2

print('proba_walker1_size256_disorder1=', list(probabilities/seeds_total_count))
      
time_set = range(500)
probabilities = np.zeros(len(time_set))

for sv in range(seeds_total_count):
    psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
    unitary_operator = QRW_topo_2D_with_defect_fixangle_disorder(size_x=sizex, size_y=sizey, topo_defect_coord = single_defect_pos, 
                                           thetaParams = [5.445427266222307, 1.591740277818828], 
                                           thetaParamsDefect = [5*np.pi/8, np.pi/2], disorder_strength=disorder_str, seed_value=sv)

    for i in time_set:
        psi = unitary_operator.dot(psi)
        probabilities[i] += np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])])**2+\
                       np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])+1])**2

print('proba_walker2_size256_disorder1=', list(probabilities/seeds_total_count))

finish_1_time = time.time()
print('done topo part size 256 dis 0.5 in time =', finish_1_time -calc_time_start)

oracle = [2]
#maveraged = np.zeros(len(time_set))
paveraged = np.zeros(len(time_set)+1)
for sv in range(seeds_total_count):
    m,p = simul_Grover_disordered(8,oracle,len(time_set), disorder_strength = disorder_str, seed_value=sv)
    paveraged += np.array(p)
    #print('done grover seed=', sv)

print('m_grover=', list(m))
print('p_grover_size256_disorder1 =', list(paveraged/seeds_total_count))
finish_2_time = time.time()
print('done grover part size 256 dis 0.5 in time =', finish_2_time -calc_time_start)


sizex = 512
sizey = 512
disorder_str = 0.5

seeds_total_count = 20
single_defect_pos = ([26], [25]) #change defect position to 16, 15 for size below 26


#psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
#print(abs(psi[8])**2+abs(psi[9])**2)

calc_time_start = time.time()

time_set = range(500)
probabilities = np.zeros(len(time_set))


for sv in range(seeds_total_count):
    psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
    unitary_operator = QRW_topo_2D_with_defect_fixangle_disorder(size_x=sizex, size_y=sizey, topo_defect_coord = single_defect_pos, 
                                           thetaParams = [0.8377580409572776, 3.853686988403478], 
                                           thetaParamsDefect = [5*np.pi/8, np.pi/2], disorder_strength=disorder_str, seed_value=sv)

    for i in time_set:
        psi = unitary_operator.dot(psi)
        probabilities[i] += np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])])**2+\
                       np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])+1])**2

print('proba_walker1_size512_disorder05=', list(probabilities/seeds_total_count))
      
time_set = range(500)
probabilities = np.zeros(len(time_set))

for sv in range(seeds_total_count):
    psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
    unitary_operator = QRW_topo_2D_with_defect_fixangle_disorder(size_x=sizex, size_y=sizey, topo_defect_coord = single_defect_pos, 
                                           thetaParams = [5.445427266222307, 1.591740277818828], 
                                           thetaParamsDefect = [5*np.pi/8, np.pi/2], disorder_strength=disorder_str, seed_value=sv)

    for i in time_set:
        psi = unitary_operator.dot(psi)
        probabilities[i] += np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])])**2+\
                       np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])+1])**2

print('proba_walker2_size512_disorder05=', list(probabilities/seeds_total_count))

finish_1_time = time.time()
print('done topo part size 512 dis 0.5 in time =', finish_1_time -calc_time_start)

oracle = [2]
#maveraged = np.zeros(len(time_set))
paveraged = np.zeros(len(time_set)+1)
for sv in range(seeds_total_count):
    m,p = simul_Grover_disordered(9,oracle,len(time_set), disorder_strength = disorder_str, seed_value=sv)
    paveraged += np.array(p)
    #print('done grover seed=', sv)

print('m_grover=', list(m))
print('p_grover_size512_disorder05 =', list(paveraged/seeds_total_count))
finish_2_time = time.time()
print('done grover part size 512 dis 0.5 in time =', finish_2_time -calc_time_start)
