import numpy as np
import scipy.sparse as sparse
import scipy.sparse.linalg as sparse_linalg
import scipy.signal
import scipy
import itertools
import multiprocessing


def QRW_topo_2D_with_defect_fixangle(size_x, size_y, topo_defect_coord, thetaParams, thetaParamsDefect):
    '''thetaParams - parameters for topo phase 1,  
       thetaParamsDefect - parameters for topo phase 2 - on one defect site,
       topo_defect_coord - tuple of (x_array, y_array) defect coordinates
       
       coordinate agreement - 2*(size_y*x+y) - coordinate of x, y up spin
    '''
    lattice_unitary_op = sparse.lil_matrix((2*size_x*size_y, 2*size_x*size_y), dtype = complex)
    for i in range(2*size_x*size_y):
        lattice_unitary_op[i, i] = 1.0
    
    lattice_unitary_op.tocsc()
    
    #print(lattice_unitary_op.todense())
    
    rotation1 = sparse.lil_matrix((2*size_x*size_y,2*size_x*size_y), dtype=complex)
    rotation2 = sparse.lil_matrix((2*size_x*size_y,2*size_x*size_y), dtype=complex)
    
    #step 1 - rotation 
    for i in range(size_x):
        for j in range(size_y):
            if (i in topo_defect_coord[0]) and (j in topo_defect_coord[1]):
                T1, T2 = thetaParamsDefect #defect
            else:
                T1, T2 = thetaParams
                
            cos1 = np.cos(T1/2)
            cos2 = np.cos(T2/2)
            sin1 = np.sin(T1/2)
            sin2 = np.sin(T2/2)

        
            rotation1[2*(size_y*i+j), 2*(size_y*i+j)] = cos1
            rotation1[2*(size_y*i+j), 2*(size_y*i+j)+1] = -sin1
            rotation1[2*(size_y*i+j) + 1, 2*(size_y*i+j)] = sin1
            rotation1[2*(size_y*i+j)+1, 2*(size_y*i+j)+1] = cos1
        
            rotation2[2*(size_y*i+j), 2*(size_y*i+j)] = cos2
            rotation2[2*(size_y*i+j), 2*(size_y*i+j)+1] = -sin2
            rotation2[2*(size_y*i+j) + 1, 2*(size_y*i+j)] = sin2
            rotation2[2*(size_y*i+j) + 1, 2*(size_y*i+j)+1] = cos2
        
    rotation1.tocsc()
    rotation2.tocsc()
    lattice_unitary_op = rotation1.dot(lattice_unitary_op)
        
    #shift 1 - along x (we impose PBC)
    shift_matrix_1 = sparse.lil_matrix((2*size_x*size_y,2*size_x*size_y), dtype=complex)
    for i in range(size_x):
        for j in range(size_y):
            if i == 0:
                shift_matrix_1[2*(size_y*i+j), 2*(size_y*(size_x-1)+j)] = 1
                shift_matrix_1[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j) + 1] = 1
            elif i == size_x - 1:
                shift_matrix_1[2*(size_y*i+j), 2*(size_y*(i-1)+j)] = 1
                shift_matrix_1[2*(size_y*i+j) + 1, 2*(size_y*(0)+j) + 1] = 1
            else:
                shift_matrix_1[2*(size_y*i+j), 2*(size_y*(i-1)+j)] = 1
                shift_matrix_1[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j) + 1] = 1
    
    shift_matrix_1.tocsc()
    lattice_unitary_op = shift_matrix_1.dot(lattice_unitary_op)
    
    #print(shift_matrix_1.dot(shift_matrix_1.T.conj()).todense())
    
    #rotation 2
    lattice_unitary_op = rotation2.dot(lattice_unitary_op)
    
    #shift 2 - along y (we impose PBC):
    shift_matrix_2 = sparse.lil_matrix((2*size_x*size_y,2*size_x*size_y), dtype=complex)
    
    for i in range(size_x):
        for j in range(size_y):
            if j == 0:
                #print(i, j, 2*(size_y*i+j), 'j=0')
                shift_matrix_2[2*(size_y*i+j), 2*(size_y*i+size_y - 1)] = 1
                shift_matrix_2[2*(size_y*i+j) + 1, 2*(size_y*i+j + 1) + 1] = 1
            elif j == size_y - 1:
                #print(i, j, 2*(size_y*i+j),'j=sizey - 1')
                shift_matrix_2[2*(size_y*i+j), 2*(size_y*i+j - 1)] = 1
                shift_matrix_2[2*(size_y*i+j) + 1, 2*(size_y*i+0) + 1] = 1
            else:
                #print(i, j, 2*(size_y*i+j),'other')
                shift_matrix_2[2*(size_y*i+j), 2*(size_y*i+j-1)] = 1
                shift_matrix_2[2*(size_y*i+j) + 1, 2*(size_y*i+j+1) + 1] = 1
    
    shift_matrix_2.tocsc()
    lattice_unitary_op = shift_matrix_2.dot(lattice_unitary_op)
    
    #print(shift_matrix_2.todense())
    #print(shift_matrix_2.dot(shift_matrix_2.T.conj()).todense())
    
    #rotation 3 - analogue of rotation 1
    lattice_unitary_op = rotation1.dot(lattice_unitary_op)
    
    #shift 3 - along y with pbc, and additional along x with pbc:
    shift_matrix_3 = sparse.lil_matrix((2*size_x*size_y,2*size_x*size_y), dtype=complex)
    
    for i in range(size_x):
        for j in range(size_y):
            if (i == 0) and (j == 0):
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(size_x-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+size_y-1) + 1] = 1
            elif (i == size_x - 1) and (j == size_y - 1):
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+0)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(0)+j-1) + 1] = 1
            elif (i == 0) and (j == size_y - 1):   
                ##
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(size_x-1)+0)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j-1) + 1] = 1
            elif (i == size_x - 1) and (j == 0):   
                ##
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(0)+size_y-1) + 1] = 1    
            elif j == 0:
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+size_y-1) + 1] = 1
            elif j == size_y - 1:
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+0)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j-1) + 1] = 1
            elif i == 0:
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(size_x-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j-1) + 1] = 1
            elif i == size_x - 1:
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(0)+j-1) + 1] = 1
            else:
                shift_matrix_3[2*(size_y*i+j), 2*(size_y*(i-1)+j+1)] = 1
                shift_matrix_3[2*(size_y*i+j) + 1, 2*(size_y*(i+1)+j-1) + 1] = 1
    
    shift_matrix_3.tocsc()
    lattice_unitary_op = shift_matrix_3.dot(lattice_unitary_op)
    
    #print(shift_matrix_3.dot(shift_matrix_3.T.conj()).todense())
    
    return lattice_unitary_op


def max_probability_10_1000(thetaParams_index):
    '''returns max probabiltiy on defect and on 0 0, 0 1 points'''

    size = 40
    thetaParamsDefect = [-np.pi, np.pi/2]
    single_defect_pos = ([26], [25])
    sizex = size
    sizey = size

    theta1_array = np.linspace(-2*np.pi, 2*np.pi, 151)
    
    thetaParams = [theta1_array[thetaParams_index[0]], theta1_array[thetaParams_index[1]]]

   
    psi = 1/np.sqrt(2*sizex*sizey)*np.ones(2*sizex*sizey)
    #print(abs(psi[8])**2+abs(psi[9])**2)
      
    time_set = range(1000)
    probabilities = np.zeros(len(time_set))
    probabilities_no_defect1 = np.zeros(len(time_set))
    probabilities_no_defect2 = np.zeros(len(time_set))

    unitary_operator = QRW_topo_2D_with_defect_fixangle(size_x=sizex, size_y=sizey, topo_defect_coord = single_defect_pos, 
                                           thetaParams = thetaParams, thetaParamsDefect = thetaParamsDefect)

    for i in time_set:
        psi = unitary_operator.dot(psi)
        probabilities[i] = np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])])**2+\
                           np.abs(psi[2*(sizey*single_defect_pos[0][0]+single_defect_pos[1][0])+1])**2

        probabilities_no_defect1[i] = np.abs(psi[0])**2+np.abs(psi[1])**2
        probabilities_no_defect2[i] = np.abs(psi[2])**2+np.abs(psi[3])**2
        
    return (thetaParams_index[0], thetaParams_index[1], np.max(probabilities[10:1000-1]), np.max(probabilities_no_defect1[10:1000-1]), np.max(probabilities_no_defect2[10:1000-1]))


if __name__=='__main__':
    #Generate values for each parameter
    theta1_array = np.linspace(-2*np.pi, 2*np.pi, 151)
    theta2_array = np.linspace(-2*np.pi, 2*np.pi, 151)
    #Generate a list of tuples where each tuple is a combination of parameters.
    #The list will contain all possible combinations of parameters.
    paramlist = list(itertools.product(list(range(151)), list(range(151))))

    pool = multiprocessing.Pool(12)

    #Distribute the parameter sets evenly across the cores
    res  = pool.map(max_probability_10_1000, paramlist)

    pool.close()
    pool.join()

    max_value = np.zeros((len(theta1_array), len(theta2_array)))
    max_value_nodef_0 = np.zeros((len(theta1_array), len(theta2_array)))
    max_value_nodef_1 = np.zeros((len(theta1_array), len(theta2_array)))

    for element in res:
        i = element[0]
        j = element[1]
        max_value[i, j], max_value_nodef_0[i, j], max_value_nodef_1[i, j] = element[2], element[3], element[4]

    with open('max_map_values_def_mpi_piover2.npy', 'wb') as f:
        for dataarray in [max_value, max_value_nodef_0, max_value_nodef_1, theta1_array, theta2_array]:
            np.save(f, dataarray)