# TopologicalSearch


## Code for simulation made in paper "Dynamical localization in 2D topological quantum random walks"
This paper is a revised version of paper arxiv:2406.18768 "Constant search time algorithm via topological quantum walks"

Authors: D. O. Oriekhov, Guliuxin Jin, Eliska Greplova

### Code files:
time_evolution_and_eigenstates_analysis_with_disorder.ipynb - Main Jupyter notebook for time evolution simulations, comparsion of 
probability with overlap criteria for eigenstates of unitary operators and comparison with 
Grover-type algorithm in non-topological quantum random walks. Contains part of the plots appearing in the paper.
The analysis of disorder role is performed in the last section.

time_evolution_and_eigenstates_analysis.ipynb - previous reference version of the time-evolution simulations code without section about disorder.

plots_for_paper_updated.ipynb - Jupyter notebook with combined plots for the revised version of the manuscript.

supplementary_plots_for_paper.ipynb - Jupyter notebook with several figures 

trapped_states_radius_calculation.ipynb - notebook with code that evaluates a radius of 80% and 95% of trapped state localization. Used for producing Fig.S5.

qrw_functions.py - supplementary file with implementation of quantum random walk and localization radius calculation function used in "trapped_states_radius_calculation.ipynb"

run_max_probability_data_generation.py - python script that was used to generate search probability maps in parameters space of random walker for a given defect. Update of parameters should be made inside script. Internally uses multiprocessing to parallelize calculation for different parameter values.

### Data files description:
'data_files' folder - a set of data files for max probability maps for different parameters of searched defect. Files stored inside were used for Figs.1 and 2 in paper, and for gif plot.

'data_radius' - separate folder for data files of trapped states localization radius data. Used for supplemental figure.

nontopo_averaged_size256.txt - a file containing the data of averaged non-topological quantum walk localization probabilities, averaged over 20 seeds.

### Python libraries versions
Python: 3.8.10
matplotlib: 3.3.4
numpy: 1.20.2
scipy: 1.6.2
